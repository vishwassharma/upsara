(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  $(function() {
    var Collection, Model, View, app_router;
    window.AL = (typeof AL !== "undefined" && AL !== null) || {};
    AL.collection = {};
    AL.model = {};
    AL.view = {};
    AL.router = {};
    Model = (function(_super) {

      __extends(Model, _super);

      function Model() {
        return Model.__super__.constructor.apply(this, arguments);
      }

      Model.prototype.nestedArray = [];

      Model.prototype.nestedModel = [];

      Model.prototype.nestedCollection = [];

      Model.prototype.initialize = function(options) {
        if (options == null) {
          options = {};
        }
        return this.id = this.get('_id');
      };

      return Model;

    })(Backbone.Model);
    Collection = (function(_super) {

      __extends(Collection, _super);

      function Collection() {
        return Collection.__super__.constructor.apply(this, arguments);
      }

      return Collection;

    })(Backbone.Collection);
    View = (function(_super) {

      __extends(View, _super);

      function View() {
        return View.__super__.constructor.apply(this, arguments);
      }

      return View;

    })(Backbone.View);
    AL.model.Group = (function(_super) {

      __extends(Group, _super);

      function Group() {
        return Group.__super__.constructor.apply(this, arguments);
      }

      Group.prototype.url = function() {
        return "/groups/" + this.id;
      };

      return Group;

    })(Model);
    AL.model.Groups = (function(_super) {

      __extends(Groups, _super);

      function Groups() {
        return Groups.__super__.constructor.apply(this, arguments);
      }

      Groups.prototype.url = '/groups';

      Groups.prototype.model = AL.model.Group;

      return Groups;

    })(Collection);
    AL.view.Group = (function(_super) {

      __extends(Group, _super);

      function Group() {
        return Group.__super__.constructor.apply(this, arguments);
      }

      Group.prototype.template = _.template($('#group-template').html());

      Group.prototype.model = AL.model.Group;

      Group.prototype.tagName = 'li';

      Group.prototype.initialize = function(options) {
        if (options == null) {
          options = {};
        }
        _.bindAll(this, 'render');
        return this.model.bind('change', this.render);
      };

      Group.prototype.render = function() {
        var context;
        context = {
          id: this.model.get('_id'),
          group_pic: this.model.get('group_pic'),
          name: this.model.get('name')
        };
        this.$el.html(this.template(context));
        return this;
      };

      return Group;

    })(View);
    AL.model.People = (function(_super) {

      __extends(People, _super);

      function People() {
        return People.__super__.constructor.apply(this, arguments);
      }

      People.prototype.initialize = function(options) {
        var groups;
        if (options == null) {
          options = {};
        }
        groups = this.get('groups');
        return this.groups = groups.map(function(group) {
          return new AL.model.Group({
            _id: group
          });
        });
      };

      People.prototype.addGroup = function(group) {
        var groupPeople;
        groupPeople = new AL.model.Group({
          _id: group
        });
        this.groups.append(groupPeople);
        return this.groups;
      };

      return People;

    })(Model);
    AL.collection.People = (function(_super) {

      __extends(People, _super);

      function People() {
        return People.__super__.constructor.apply(this, arguments);
      }

      People.prototype.url = '/people';

      People.prototype.model = AL.model.People;

      return People;

    })(Collection);
    AL.view.Person = (function(_super) {

      __extends(Person, _super);

      function Person() {
        return Person.__super__.constructor.apply(this, arguments);
      }

      Person.prototype.template = _.template($('#person-template').html());

      Person.prototype.tagName = 'article';

      Person.prototype.initialize = function(options) {
        var self;
        if (options == null) {
          options = {};
        }
        _.bindAll(this, 'render');
        this.model.bind('change', this.render);
        self = this;
        return this.groupView = _.map(this.model.groups, function(model) {
          var view;
          view = new AL.view.Group({
            model: model,
            el: '#profile'
          });
          view.parent = self;
          return view;
        });
      };

      Person.prototype.render = function() {
        var context;
        context = {
          firstname: this.model.get('firstname'),
          lastname: this.model.get('lastname'),
          profile_pic: "http://placehold.it/60X60",
          description: this.model.get('description'),
          roles: this.model.get('roles')
        };
        this.$el.html(this.template(context));
        this.renderGroups();
        return this;
      };

      Person.prototype.renderGroups = function() {
        var x;
        return x = _.map(this.groupView, function(view) {
          view.model.fetch();
          return console.log(view);
        });
      };

      return Person;

    })(View);
    AL.view.PeopleForm = (function(_super) {

      __extends(PeopleForm, _super);

      function PeopleForm() {
        return PeopleForm.__super__.constructor.apply(this, arguments);
      }

      PeopleForm.prototype.template = _.template($("#people-form").html());

      PeopleForm.prototype.tagName = 'article';

      PeopleForm.prototype.render = function() {
        this.$el.html(this.template({}));
        return this;
      };

      return PeopleForm;

    })(View);
    AL.view.People = (function(_super) {

      __extends(People, _super);

      function People() {
        return People.__super__.constructor.apply(this, arguments);
      }

      People.prototype.template = _.template($('#people-template').html());

      People.prototype.el = '#content';

      People.prototype.initialize = function(options) {
        if (options == null) {
          options = {};
        }
        _.bindAll(this, 'render', 'addOne', 'addAll');
        this.collection.bind('add', this.addOne);
        this.collection.bind('reset', this.addAll);
        return this.collection.fetch();
      };

      People.prototype.render = function() {
        this.$el.html(this.template({}));
        return this;
      };

      People.prototype.addOne = function(model) {
        var modelView;
        modelView = new AL.view.Person({
          model: model
        });
        return this.$("#people-container").append(modelView.render().el);
      };

      People.prototype.addAll = function() {
        var formView;
        this.$("#people-container").empty();
        this.collection.each(this.addOne);
        formView = new AL.view.PeopleForm;
        return this.$("#people-container").append(formView.render().el);
      };

      return People;

    })(View);
    AL.router.MainRouter = (function(_super) {

      __extends(MainRouter, _super);

      function MainRouter() {
        return MainRouter.__super__.constructor.apply(this, arguments);
      }

      MainRouter.prototype.routes = {
        '': 'home',
        'startups': 'startups',
        'talents': 'talents',
        'angels': 'angels',
        'groups/:id': 'groups',
        'people/:lastname/:firstname': 'people'
      };

      MainRouter.prototype.home = function() {
        var col, view;
        console.log("home page");
        col = new AL.collection.People;
        view = new AL.view.People({
          collection: col
        });
        return view.render();
      };

      MainRouter.prototype.startups = function() {
        return console.log("startups page");
      };

      MainRouter.prototype.talents = function() {
        return console.log("talents page");
      };

      MainRouter.prototype.angels = function() {
        return console.log("Angels Page");
      };

      MainRouter.prototype.groups = function(id) {
        return console.log("Group page");
      };

      MainRouter.prototype.people = function(lastname, firstname) {
        return console.log("Searching for " + lastname + " and " + firstname);
      };

      return MainRouter;

    })(Backbone.Router);
    app_router = new AL.router.MainRouter;
    return Backbone.history.start();
  });

}).call(this);
