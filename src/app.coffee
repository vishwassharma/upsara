$ ->
    # Define AL - Angelist as a global object
    window.AL = AL? or {}

    AL.collection = {}
    AL.model = {}
    AL.view = {}
    AL.router = {}

    # Define the basic bones of backbone model
    class Model extends Backbone.Model

        nestedArray : []

        nestedModel : []

        nestedCollection : []

        initialize : (options = {}) ->
            @id = @get('_id')

    class Collection extends Backbone.Collection
        
    class View extends Backbone.View

    # =================================================
    # Groups and its member
    # =================================================
    class AL.model.Group extends Model

        url : () ->
            "/groups/#{@id}"

    class AL.model.Groups extends Collection

        url : '/groups'

        model : AL.model.Group


    class AL.view.Group extends View

        template : _.template $('#group-template').html()

        model : AL.model.Group

        tagName : 'li'

        initialize : (options = {}) ->
            _.bindAll @, 'render'
            @model.bind 'change', @render

        render: () ->
            context=
                id : @model.get '_id'
                group_pic : @model.get 'group_pic'
                name : @model.get 'name'
            @$el.html @template context
            @
            

    # ==================================================
    # People and Person Setup
    # ==================================================


    class AL.model.People extends Model

        initialize : (options = {}) ->
            groups = @get 'groups'
            @groups = groups.map (group) ->
                        new AL.model.Group _id : group

        addGroup : (group) ->
            groupPeople = new AL.model.Group _id : group
            @groups.append groupPeople
            @groups

    class AL.collection.People extends Collection

        url : '/people'

        model : AL.model.People

    class AL.view.Person extends View

        template : _.template $('#person-template').html()

        tagName : 'article'

        initialize : (options = {}) ->
            _.bindAll @, 'render'
            @model.bind 'change', @render
            self = @

            @groupView = _.map @model.groups, (model) ->
                view = new AL.view.Group
                        model : model
                        el : '#profile'
                view.parent = self
                view

        render: ->
            context=
                firstname : @model.get 'firstname'
                lastname : @model.get 'lastname'
                profile_pic : "http://placehold.it/60X60"
                description : @model.get 'description'
                roles : @model.get 'roles'
            @$el.html @template context
            @renderGroups()
            @

        renderGroups : ->
            x = _.map @groupView, (view) ->
                view.model.fetch()
                console.log view

    class AL.view.PeopleForm extends View

        template : _.template $("#people-form").html()

        tagName : 'article'

        render : ->
            @$el.html @template {}
            @

    class AL.view.People extends View

        template : _.template $('#people-template').html()

        el : '#content'

        initialize : (options = {}) ->
            _.bindAll @, 'render', 'addOne', 'addAll'
            @collection.bind 'add', @addOne
            @collection.bind 'reset', @addAll
            @collection.fetch()

        render : ->
            @$el.html @template {}
            @
        
        addOne : (model) ->
            modelView = new AL.view.Person model : model
            @$("#people-container").append modelView.render().el

        addAll : ->
            @$("#people-container").empty()
            @collection.each @addOne
            formView = new AL.view.PeopleForm
            @$("#people-container").append formView.render().el


    class AL.router.MainRouter extends Backbone.Router
    
        routes:
            '': 'home'
            'startups': 'startups'
            'talents': 'talents'
            'angels': 'angels'

            # main page
            'groups/:id' : 'groups'
            'people/:lastname/:firstname' : 'people'
        
        home: ->
            console.log "home page"
            col = new AL.collection.People
            view = new AL.view.People
                        collection : col
            view.render()


        startups : ->
            console.log "startups page"

        talents : ->
            console.log "talents page"

        angels : ->
            console.log "Angels Page"

        groups : (id) ->
            console.log "Group page"

        people : (lastname, firstname) ->
            console.log "Searching for #{lastname} and #{firstname}"

    app_router = new AL.router.MainRouter
    Backbone.history.start()
