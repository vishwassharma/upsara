# import dependencies
express = require "express"
path = require "path"
apps = require "./apps"

# global settings
port = 9000
app_root = __dirname

# create server
app = module.exports = express.createServer()

# server configuration
app.configure () ->
    app.use express.bodyParser()
    app.use express.methodOverride()
    app.use express.cookieParser()
    app.use express.session secret : '83809LKSJFDS09DOI32J423098OKD'
    app.use app.router
    # view options
    app.set 'views', path.join(app_root, 'public')
    #app.set 'view engine', 'jade'
    app.set 'view options', layout : false
    # static files
    app.use express.static path.join(app_root, 'public')

app.register '.html',
    compile : (str, options) ->
        (locals) ->
            str

# map the routes with the callback function
app.get '/', apps.index
app.get '/people', apps.people.views.get
app.post '/people', apps.people.views.post
app.get '/people/:id' , apps.people.views.getById
app.get '/people/:id/:feature', apps.people.views.getFeatureById
app.get '/groups', apps.groups.views.get
app.get '/groups/:id', apps.groups.views.getById
app.get '/groups/:id/:feature', apps.groups.views.getFeatureById
# people delete

app.listen port, (event) ->
    console.log "[*] Server listening on #{port}"
