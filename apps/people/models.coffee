mongoose = require "mongoose"
Schema = mongoose.Schema
ObjectId = Schema.ObjectId

PersonSchema = new Schema
            firstname : String
            lastname : String
            profile_pic : String
            roles : [String]
            description : String
            groups : [{type: ObjectId, ref: 'Group'}]


Person = exports.Person = mongoose.model('Person', PersonSchema)
#Test = exports.Test = mongoose.model('Test', TestSchema)

#a= new Person()
#a.firstname = "Amit"
#a.lastname = "Sharma"
#a.roles = ["investor"]
#a.groups = ["4fce410b9e6bbdd426000001", "4fce41b5a30a55e826000001"]
#a.description = "I started my job in Aon, after few years I switched to goldman Sachs. Currently I am in IIM C"
#a.save()
