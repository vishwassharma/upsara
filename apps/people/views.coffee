models = require './models'

get = (req, res) ->
    # get the list of all the persons in the database
    models.Person.find {}, (err, docs) ->
        res.json docs

post = (req, res) ->

    res.send 'PersonView#post page'

del = (req, res) ->
    models.Person.find {}, (err, docs) ->
        for doc in docs
            doc.remove()

getById = (req, res) ->
    res.send "Get by Id"

getFeatureById = (req, res) ->
    res.send "Get feature by Id"
            
exports.get = get
exports.post = post
exports.del = del
exports.getById = getById
exports.getFeatureById = getFeatureById
