models = require './models'

get = (req, res) ->
    # get the list of all the persons in the database
    models.Person.find {}, (err, docs) ->
        res.json docs

post = (req, res) ->

    res.send 'PersonView#post page'

del = (req, res) ->
    models.Person.find {}, (err, docs) ->
        for doc in docs
            doc.remove()

exports.get = get
exports.post = post
exports.del = del
